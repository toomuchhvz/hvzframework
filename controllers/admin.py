# coding: utf8

# Admin page
@auth.requires_membership('admins')
def index(): return dict(message="hello from admin.py")


# admin squad management interface
@auth.requires_membership('admins')
def adminsquadlist():
    response.view='default.html'
    grid = SQLFORM.grid(db.squads, csv=False)
    return dict(form=grid)


# admin player-type management interface
@auth.requires_membership('admins')
def manage_types():
    response.view='default.html'
    grid = SQLFORM.grid(db.creature_type, csv=False)
    return dict(form=grid)


# admin game participation management interface
@auth.requires_membership('admins')
def manage_gameparts():
    response.view='default.html'
    grid = SQLFORM.grid(db.game_part, csv=False)
    return dict(form=grid)


# post management interface
@auth.requires_membership('admins')
def manage_posts():
    response.view='default.html'
    grid = SQLFORM.grid(db.posts, csv=False)
    return dict(form=grid)


# admin user management interface
@auth.requires_membership('admins')
def manage_users():
    response.view='default.html'
    form = SQLFORM.smartgrid(db.auth_user,linked_tables=['game_part'], csv=False)
    return dict(form=form)


# sassy post management interface
@auth.requires_membership('admins')
def sassy_posts():
    response.view='default.html'
    grid = SQLFORM.grid(db.sassypost, csv=False)
    return dict(form=grid)


# The mission list page
@auth.requires_membership('admins')
def missionlist():
    response.view='default.html'
    grid = SQLFORM.grid(db.missions, csv=False)
    return dict(form=grid)


# The cure list page
@auth.requires_membership('admins')
def curelist():
    cures = db(db.cures).select(orderby=db.cures.game_id)
    return dict(cures=cures)


# The flavorimages index page
@auth.requires_membership('admins')
def flavorimages():
    response.view='default.html'
    grid = SQLFORM.grid(db.cycleimages, csv=False)
    return dict(form=grid)


# takes a zombie game_part id and adds the food to it
@auth.requires_membership('admins')
def zombierewardfood():
    form = SQLFORM.factory(Field("Game", default=currentgame()),Field("Minutes", 'integer'),submit_button="Add Time!!",)
    if form.process().accepted:
        realtime = (form.vars.Minutes * 60)
        zombies=db((db.game_part.game_id==form.vars.Game) & (db.game_part.creature_type!=1)).select()
        for zombie in zombies:
            if not isdead(zombie):
                stime = datetime.timedelta(seconds=realtime)
                newtime = zombie.zombie_expires_at + stime
                db(db.game_part.id==zombie.id).update(zombie_expires_at=newtime)
            status= "All Zombies Given " + str(realtime) + " minutes of life!"
        return dict(form=[], status=status)
    return dict(form=form, status="Enter the Minutes to reward")


# Shambling dead interface 
@auth.requires_membership('admins')
def zombieraise():
    import random
    response.view='admin/zombierewardfood.html'
    form = SQLFORM.factory(Field("Game", default=currentgame()),Field("confirm", label="type: I AM SURE to confirm", default="" ),submit_button="RAISE THE DEAD!",)
    if form.process().accepted:
        if form.vars.confirm == "I AM SURE":
            zombies=db((db.game_part.game_id==form.vars.Game) & (db.game_part.creature_type!=1)).select()
            for zombie in zombies:
                if isdead(zombie):
                    stime = datetime.timedelta(seconds=random.randrange(10800,43200))
                    newtime = request.now + stime
                    db(db.game_part.id==zombie.id).update(zombie_expires_at=newtime)
            status= "THE SHAMBLING DEAD HAVE ARISEN ONCE AGAIN!"
        return dict(form=[], status=status)
    return dict(form=form, status="Confirm the raising of the dead? All dead will be raised and given random amounts of food.")


# edit a cure page
@auth.requires_membership('admins')
def editcure():
    response.view='default.html'
    cure = db.cures(request.args(0)) or redirect(URL('error'))
    form=SQLFORM(db.cures, cure, deletable = True)
    if form.validate():
        if form.deleted:
            db(db.cures.id==cure.id).delete()
            redirect(URL('curelist'))
            response.flash = 'GAME BALEETED!'
        else:
            cure.update_record(**dict(form.vars))
            response.flash = 'Changes saved.'
    else:
        response.flash = 'Edit the cure.'
    return dict(form=form)


# Create a cure function
@auth.requires_membership('admins')
def createcure():
    if not currentgame():
        response.flash = 'Can only create a cure when there is a game active'
        redirect(URL(c='admin', f='curelist'))
    else:
        db.cures.insert(game_id=currentgame(), curecode=generatecurecode(), used=False, expiration=zombiebitefood(request.now))
        response.flash = 'Cure code created!'
        redirect(URL(c='admin', f='curelist'))

        
# The admin game page
@auth.requires_membership('admins')
def games():
    games = db(db.games).select(orderby=db.games.created)
    return dict(games=games)


# The create a game page
@auth.requires_membership('admins')
def creategame():
    response.view='default.html'
    form=SQLFORM(db.games)
    if form.process().accepted:
        response.flash = 'game created!'
    elif form.errors:
        response.flash = 'The game has errors, idiot!'
    else:
        response.flash = 'Make your game!'
    return dict(form=form)


# edit a game page
@auth.requires_membership('admins')
def editgame():
    response.view='default.html'
    game = db.games(request.args(0)) or redirect(URL('error'))
    form=SQLFORM(db.games, game, deletable = True)
    response.flash = 'Edit the game'
    if form.validate():
        if form.deleted:
            db(db.games.id==game.id).delete()
            redirect(URL('games'))
            response.flash = 'GAME BALEETED!'
        else:
            game.update_record(**dict(form.vars))
            response.flash = 'Changes saved.'
    else:
        response.flash = 'Edit the game!'
    return dict(form=form)


# View the game_part's in a game
@auth.requires_membership('admins')
def viewgameusers():
    gid = request.args[0]
    if db.games(gid):
        if request.args[1]:
            page=int(request.args[1])
        else: page=0
        items_per_page=40
        limitby=(page*items_per_page,(page+1)*items_per_page+1)
        response.flash = db.games(gid).game_name
        users = db(db.game_part.game_id == gid).select(orderby=db.game_part.id,limitby=limitby)
        return dict(users=users,page=page,items_per_page=items_per_page, gid=gid)
    else:
        redirect(URL(c='admin', f='games'))

        
# View the oz pool for a game
@auth.requires_membership('admins')
def ozlist():
    response.flash = db.games(request.args(0)).game_name
    gid = request.args(0)
    users=db(db.game_part.game_id == gid and db.game_part.original_request).select(db.game_part.ALL)
    return dict(users=users)


# Create a game_part for a game
@auth.requires_membership('admins')
def creategamepart():
    response.view='default.html'
    form=SQLFORM(db.game_part)
    if form.process().accepted:
        response.flash = 'Game participation created'
    elif form.errors:
        response.flash = 'The game has errors, idiot!'
    return dict(form=form)


# Edit a game_part
@auth.requires_membership('admins')
def editgamepart():
    response.view='default.html'
    part = db.game_part(request.args(0)) or redirect(URL('error'))
    form=SQLFORM(db.game_part, part, deletable = True)
    if form.validate():
        if form.deleted:
            db(db.game_part.id==part.id).delete()
            redirect(URL('games'))
            response.flash = 'GAME BALEETED!'
        else:
            part.update_record(**dict(form.vars))
            response.flash = 'Changes saved.'
    else:
        response.flash = 'Edit the participation'
    return dict(form=form,)


# reset a bitecode function
@auth.requires_membership('admins')
def regencode():
    if request.args[0]:
         gid = request.args[0]
         db(db.game_part.id==gid).update(bitecode=generatebitecode())
         try:
              game=request.args[1]
              page=request.args[2]
         except:
              game=1
              page=0
         redirect(URL(c='admin', f='viewgameusers', args=[game,page]))
    else:
        redirect(URL(c='admin', f='games'))

        
# Makes a user an OZ
@auth.requires_membership('admins')
def makeoz():
    db(db.game_part.id==request.args(0)).update(creature_type=3)
    userpart = db(db.game_part.id==request.args(0)).select()
    response.flash='made them an OZ!'
    redirect(URL(c='admin', f='ozlist', args=userpart[0].game_id))


# controller for the the open squad applications page
@auth.requires_membership('admins')
def squadapplist():
    squadsapp=db(db.squads_app.reviewed=="False").select()
    return dict(squadsapp=squadsapp)


# This function is for the admin squad review page. It rejects a squad application.
@auth.requires_membership('admins')
def denysquadapp():
    sappid=request.args[0]
    db(db.squads_app.id==sappid).update(reviewed=True,reviewer=auth.user.id)
    redirect(URL(c='admin', f='squadapplist'))

    
# This function is for the admin squad review page. It approves a squad application and creates it. It also makes the applicant the squad leader.
@auth.requires_membership('admins')
def approvesquadapp():
   sappid=request.args[0]
   sq=db.squads_app(sappid)
   db(db.squads_app.id==sappid).update(reviewed=True,approved=True,reviewer=auth.user.id)
   db.squads.insert(game_id=sq.game_id,name=sq.name,description=sq.description,image=sq.image,leader=sq.leader, sigid=sq.sigid)
   search = db.squads.sigid.like(sq.sigid)
   results = db(search).select().first()
   db(db.game_part.id==sq.leader).update(squad_leader=True, squad_id=results.id)
   redirect(URL(c='admin', f='squadapplist'))

    
# controller for the the open registration requests page
@auth.requires_membership('admins')
def regrequestlist():
    regreq=db(db.registration_request.reviewed=='False').select()
    return dict(regreq=regreq)


# This function is for the admin reg request review page. It rejects a reg request.
@auth.requires_membership('admins')
def denyregreq():
    regreqid=request.args[0]
    db(db.registration_request.id==regreqid).update(reviewed=True,approved=False,reviewer=auth.user.id)
    redirect(URL(c='admin', f='regrequestlist'))

    
# This function is for the admin reg request review page. It approves a reg reqest and creates a valid registration app.
@auth.requires_membership('admins')
def approveregreq():
   regcode = generatebitecode()
   regreqid=request.args[0]
   regreq = db.registration_request(regreqid)
   db(db.registration_request.id==regreqid).update(reviewed=True,approved=True,reviewer=auth.user.id)
   db.registration_app.insert(user_id=regreq.user_id, game_id=regreq.game_id, registration_code=regcode, original_request=regreq.original_request, created=request.now, registration_email=regreq.registration_email)
   message = "The Admins have approved your registration request! This is your registration code: * " + regcode + " *. Head to http://umasshvz.com to use it."
   sendemail(regreq.registration_email, "HvZ Registration code", message)
   redirect(URL(c='admin', f='regrequestlist'))
