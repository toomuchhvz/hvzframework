# -*- coding: utf-8 -*-

response.static_version = '1.0.0'

authstat = auth.is_logged_in()
# The logo/header
response.logo = H1(A(B('UMASSHvZ'),_class="topmenu",_href=URL('default', 'index')))

## read more at http://dev.w3.org/html5/markup/meta.name.html
response.meta.author = 'Ed Clairmont <jeclairm@gmail.com>'
response.meta.description = 'hvzframework'
response.meta.keywords = 'web2py, python, framework'
response.meta.generator = 'Web2py Web Framework'

## your http://google.com/analytics id
response.google_analytics_id = None

# if not authstat:
#    response.menu = [(SPAN('You need to Login', _class='headerhvz'), False, URL('default', 'index'), []),]


# ---- Status menu logic begin ----


# Human menu
if returncurrentuserpart():
    gpart=returncurrentuserpart()
    zombiestat=db.creature_type(gpart.creature_type).zombie
    deadstat=isdead(gpart)
    stime=pretty_date(gpart.zombie_expires_at)
    leaderstat=gpart.squad_leader
    squadstat=gpart.squad_id
    if authstat and not zombiestat and not gpart.banned:
        bcode = gpart.bitecode
        ctype = db.creature_type(gpart.creature_type).name
        response.statusmenu = [
                (SPAN(ctype, _class='headerhvz'), False, URL('default', 'index'),[
                (T('Bitecode'), False, URL('gamectrl', 'bitecodeqrcodepage'), [
                (T(bcode), False, URL('gamectrl', 'bitecodeqrcodepage')),
                ]),
                ]),    
            ]
# Zombie menus   
    elif authstat and zombiestat and not gpart.banned:
        ctype = db.creature_type(gpart.creature_type).name
        
        # non-immortal Zombie menu   
        if not db.creature_type(gpart.creature_type).immortal and not deadstat:
            response.statusmenu = [
                (SPAN(ctype, _class='headerhvz'), False, URL('default', 'index'),[
                (T('Bite Somebody'), False, URL('gamectrl', 'bitecodepg')),
                (T('Cure'), False, URL('gamectrl', 'curezombie')),
                (T('Starve: ' +stime), False, URL('default', 'index')),
                ]),
            ]
            
            # Immortal zombie menu  
        elif db.creature_type(gpart.creature_type).immortal and not gpart.banned: 
            response.statusmenu = [
                (SPAN(ctype, _class='headerhvz'), False, URL('default', 'index'),[
                (T('Bite Somebody'), False, URL('gamectrl', 'bitecodepg')),
                (T('Cure'), False, URL('gamectrl', 'curezombie')), 
                ]),
            ]
            
            # Starved menu            
        elif deadstat and not gpart.banned:
               response.statusmenu = [(SPAN('Dead', _class='headerhvz'), False, URL('default', 'index'),[
                (T(stime), False, URL('default', 'index')),
                ]),
            ]
                
# Squad leader menu                
    if leaderstat and squadstat and not gpart.banned:
        response.squadmenu = [
            (SPAN('Squad', _class='headerhvz'), False, URL('gamectrl', 'squadhq', args=gpart.squad_id), [
            (T('Squad HQ'), False, URL('gamectrl', 'squadhq', args=gpart.squad_id)),
            (T('Manage Squad'), False, URL('gamectrl', 'squadadmin', args=gpart.squad_id)),
        ]),
        ]
        
# Squad member menu         
    elif squadstat and not leaderstat and not gpart.banned:
        response.squadmenu = [
            (SPAN('Squad', _class='headerhvz'), False, URL('gamectrl', 'squadhq', args=gpart.squad_id), [
            (T('Squad HQ'), False, URL('gamectrl', 'squadhq', args=gpart.squad_id)),
        ]),
        ]
# Menu if banned   
    elif gpart.banned:
        response.statusmenu = [
         (SPAN('BANNED', _class='headerhvz'), False, URL('default', 'index'),),]    
        
# Menu if not registered for upcoming or current game
elif not returncurrentuserpart() and currentgame() or isgameupcoming():
    response.statusmenu = [
         (SPAN('Register for the game!', _class='headerhvz'), False, URL('gamectrl', 'register'),),

]    
# ---- End Status menu logic ----
    

                        
# Generic HvZ Menu        
response.menu = [
            (SPAN('Game', _class='headerhvz'), False, URL('default', 'index'), [
            (T('Game Roster'), False, URL('default', 'roster'), [
            (T('Humans'), False, URL('default', 'rosterhumans')),
            (T('Zombies'), False, URL('default', 'rosterzombies')),
            (T('Graveyard'), False, URL('default', 'rosterdead')),
            ]),
            (T('Forums'), False, URL('forums', 'viewcat', args=1), []),
            (T('Squads'), False, URL('default', 'squadlist'), [
            (T('Squad List'), False, URL('default', 'squadlist')),
            (T('Create a Squad'), False, URL('gamectrl', 'createsquadapp')),
            ]),
            (T('Ruleset'), False, URL('default', 'rules')),
            (T('Toggle Pinktext'), False, URL('gamectrl', 'pinky')),
            ]),
]

# Admin menu for logged in admins      
if authstat and auth.has_membership('admins'):
        response.adminmenu = [
            (SPAN('Admin', _class='headerhvz'), False, URL('admin', 'index'), [
            (T('Games'), False, URL('admin', 'games')),
            
            (T('Manage Posts'), False, URL('admin', 'manage_posts')),
            (T('Manage Users'), False, URL('admin', 'manage_users')),
            (T('Reg Requests'), False, URL('admin', 'regrequestlist')),
            (T('Squad Management'), False, URL('admin', 'index'), [
            (T('Squad List'), False, URL('admin', 'adminsquadlist')),
            (T('Squad Apps'), False, URL('admin', 'squadapplist')),
            ]),
            (T('Cure List'), False, URL('admin', 'curelist')),
            (T('Missions'), False, URL('admin', 'missionlist')),
            (T('Reward Zombies'), False, URL('admin', 'zombierewardfood')),
            (T('SHAMBLING DEAD'), False, URL('admin', 'zombieraise')),
            (T('Creature Types'), False, URL('admin', 'manage_types')),
			(T('Sassy Messages'), False, URL('admin', 'sassy_posts')),
            
            
            ]),
        ]
        
# disables adminmenu for non-admins        
else: 
    response.adminmenu = ' '

# landing page link
response.landing = [(SPAN('UMASSHvZ', _class='headerhvz'), False, URL('default', 'index'),[]),]
